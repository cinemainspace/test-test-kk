using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;

namespace BookLibrary
{
    /// <summary>
    ///  Тесты на работу с очередями (Enqueue)
    /// </summary>
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public sealed class BookLibraryTestsTask
    {
        [OneTimeSetUp]
        public async Task InitializeParametersBeforeTests()
        {
            // перед тестами создаем предворительно рандомные названия книг и читателей
            _bookOne = TestContext.CurrentContext.Random.GetString();
            _bookTwo = TestContext.CurrentContext.Random.GetString();

            _readerOne = TestContext.CurrentContext.Random.GetString();
            _readerTwo = TestContext.CurrentContext.Random.GetString();
            _readerThree = TestContext.CurrentContext.Random.GetString();

            // Также предворительно проверяем, что создается очередь из одной книги и читателя
            // Если нельзя создать -- следующие тесты не имеют смысла запускать

            // Arrange
            var bookLibrary = await CreateBookLibrary();

            var idBook = bookLibrary.AddBook(book: new Book(_bookOne));
            bookLibrary.CheckoutBook(bookId: idBook, userName: _readerOne);

            // Act
            bookLibrary.Enqueue(bookId: idBook, userName: _readerTwo);
            var book = bookLibrary.GetBookById(idBook);

            // Assert
            const int expectedLength = 1;
            book.Queue.Count.Should().Be(expectedLength, because: $"Ожидаемая длина очереди == '{expectedLength}'");
            book.Queue.Should().Contain(_readerTwo, because: $"Ожидается пользователь == '{_readerOne}'");
        }

        [Test(
            Description = "Можно создать очередь из 2 читателй за одну книгу"
        )]
        public async Task Test_EnqueueFromOneBookFewReader()
        {
            // Arrange
            var bookLibrary = await CreateBookLibrary();

            var idBook = bookLibrary.AddBook(book: new Book(_bookOne));
            bookLibrary.CheckoutBook(bookId: idBook, userName: _readerOne);

            // Act
            bookLibrary.Enqueue(bookId: idBook, userName: _readerTwo);
            bookLibrary.Enqueue(bookId: idBook, userName: _readerThree);

            var book = bookLibrary.GetBookById(idBook);

            // Assert
            const int expectedLength = 2;
            book.Queue.Count.Should().Be(expectedLength, because: $"Ожидаемая длина очереди == '{expectedLength}'");
            book.Queue.First().Should()
                .Contain(_readerTwo, because: $"Ожидается, что есть пользователь == '{_readerTwo}'");
            book.Queue.Should().Contain(_readerThree, because: $"Ожидается, что есть пользователь == '{_readerThree}'");
        }


        [Test(
            Description = "Можно создать две очереди за разными книгами"
        )]
        public async Task Test_UserTwoEnqueueTwoBook()
        {
            // Arrange
            var bookLibrary = await CreateBookLibrary();

            var idBook1 = bookLibrary.AddBook(book: new Book(title: _bookOne));
            var idBook2 = bookLibrary.AddBook(book: new Book(title: _bookTwo));

            bookLibrary.CheckoutBook(bookId: idBook1, userName: _readerOne);
            bookLibrary.CheckoutBook(bookId: idBook2, userName: _readerTwo);

            // Act
            bookLibrary.Enqueue(bookId: idBook1, userName: _readerThree);
            bookLibrary.Enqueue(bookId: idBook2, userName: _readerThree);

            var book1 = bookLibrary.GetBookById(bookId: idBook1);
            var book2 = bookLibrary.GetBookById(bookId: idBook2);

            // Assert
            book1.Queue.First().Should()
                .Be(_readerThree, because: $"Ожидается, что первый в очереди будет '{_readerThree}'");
            book2.Queue.First().Should()
                .Be(_readerThree, because: $"Ожидается, что первый в очереди будет '{_readerThree}'");
        }

        [Test(
            Description = "Нельзя создать очередь без книги"
        )]
        public async Task Test_CannotEnqueueBookNull()
        {
            // Arrange
            var bookLibrary = await CreateBookLibrary();

            // Act
            Action action = () => bookLibrary.Enqueue(bookId: new Guid(), userName: _readerOne);

            // Assert
            action.Should().Throw<Exception>(because: "Ожидается, что нельзя создать очередь без книги");
        }

        [Test(
            Description = "Нельзя создать очередь с пустым читателем"
        )]
        public async Task Test_NoCreateQueueWithUsernameNull()
        {
            // Arrange
            var bookLibrary = await CreateBookLibrary();

            var idBook = bookLibrary.AddBook(book: new Book(title: _bookOne));
            bookLibrary.CheckoutBook(bookId: idBook, userName: _readerOne);

            // Act
            Action action = () => bookLibrary.Enqueue(bookId: idBook, userName: _readerNull);

            // Assert
            action.Should()
                .Throw<ArgumentNullException>(because: "Ожидается, что нельзя создать очередь с пустым пользователем");
        }

        [Test(
            Description = "Нельзя добавить пользователя в очередь, если он удерживает книгу"
        )]
        public async Task Test_CannotEnqueueIfUserHoldsBook()
        {
            // Arrange
            var bookLibrary = await CreateBookLibrary();

            var idBook = bookLibrary.AddBook(book: new Book(_bookOne));
            bookLibrary.CheckoutBook(bookId: idBook, userName: _readerOne);

            var book = bookLibrary.GetBookById(bookId: idBook);

            // Act
            Action action = () => bookLibrary.Enqueue(bookId: idBook, userName: _readerOne);

            // Assert
            action.Should()
                .Throw<BookLibraryException>(
                    because: "Ожидается, что нельзя добавить читателя в очередь, когда книга у него самого")
                .WithMessage(
                    $"Cannot enqueue user '{_readerOne}' for book '{book.Book.Title}' with id '{book.BookId}', which user holds");
        }

        [Test(
            Description = "Нельзя создать очередь если книга свободна"
        )]
        public async Task Test_CannotEnqueueBookFree()
        {
            // Arrange
            var bookLibrary = await CreateBookLibrary();
            var idBook = bookLibrary.AddBook(book: new Book(_bookOne));

            // Act
            Action action = () => bookLibrary.Enqueue(bookId: idBook, userName: _readerOne);

            // Assert
            action.Should()
                .Throw<BookLibraryException>(because: "Ожидается, что нельзя создать очередь, если книга свободна")
                .WithMessage($"Cannot enqueue if book is free and queue is empty. Checkout book instead.");
        }


        [Test(
            Description = "Нельзя встать в очередь, если уже состоишь в ней"
        )]
        public async Task Test_CannotEnqueueAlreadyInQueue()
        {
            // Arrange
            var bookLibrary = await CreateBookLibrary();

            var idBook = bookLibrary.AddBook(book: new Book(_bookOne));
            bookLibrary.CheckoutBook(bookId: idBook, userName: _readerOne);
            bookLibrary.Enqueue(bookId: idBook, userName: _readerTwo);

            // Act
            Action action = () => bookLibrary.Enqueue(bookId: idBook, userName: _readerTwo);

            // Assert
            action.Should()
                .Throw<BookLibraryException>(because: "Ожидается, что нельзя встать в очередь, если уже состоишь в ней")
                .WithMessage($"User '{_readerTwo}' is already in queue");
        }

        [Test(
            Description = "Можно встать в очередь за книгой, после того как ее вернув"
        )]
        public async Task Test_ReturnBookAndEnqueue()
        {
            // Arrange
            var bookLibrary = await CreateBookLibrary();
            var idBook = bookLibrary.AddBook(new Book(_bookOne));

            bookLibrary.CheckoutBook(bookId: idBook, userName: _readerOne);
            bookLibrary.Enqueue(bookId: idBook, userName: _readerTwo);
            bookLibrary.Enqueue(bookId: idBook, userName: _readerThree);

            // Act
            bookLibrary.ReturnBook(bookId: idBook);
            bookLibrary.Enqueue(bookId: idBook, userName: _readerOne);
            var book = bookLibrary.GetBookById(idBook);

            // Assert
            book.Queue.Should().Contain(_readerOne, because: "Ожидается, что можно встать в очередь после сдачи книги");
        }

        [Test(
            Description = "Очередь должна быть пустой, если взять, а после вернуть книгу"
        )]
        public async Task Test_EnqueueNullAfterReturnBook()
        {
            // Arrange
            var bookLibrary = await CreateBookLibrary();
            var idBook = bookLibrary.AddBook(new Book(_bookOne));

            bookLibrary.CheckoutBook(bookId: idBook, userName: _readerOne);
            bookLibrary.Enqueue(bookId: idBook, userName: _readerTwo);

            // Act
            bookLibrary.ReturnBook(bookId: idBook);
            bookLibrary.CheckoutBook(bookId: idBook, userName: _readerTwo);
            var book = bookLibrary.GetBookById(bookId: idBook);

            // Assert
            book.Queue.Count.Should().Be(0, because: "Ожидается, что очередь будет 0");
        }

        // Всякое необходимое в тестах
        private string _bookOne;
        private string _bookTwo;

        private readonly string _readerNull = null;
        private string _readerOne;
        private string _readerTwo;
        private string _readerThree;

        private static async Task<BookLibrary> CreateBookLibrary()
        {
            TestContext.WriteLine("Create Book Library");
            return await Task.FromResult(new BookLibrary());
        }
    }
}